function MyProfile() {
}

MyProfile.prototype.profile = function (options, successCallback, errorCallback) {
  cordova.exec(successCallback, errorCallback, "MyProfile", "profile", [options]);
};

MyProfile.prototype.thumbnail = function (options, successCallback, errorCallback) {
  cordova.exec(successCallback, errorCallback, "MyProfile", "thumbnail", [options]);
};

MyProfile.install = function () {
  if (!window.plugins) {
    window.plugins = {};
  }

  window.plugins.myprofile = new MyProfile();
  return window.plugins.myprofile;
};

cordova.addConstructor(MyProfile.install);