package com.sixgreen.plugins.myprofile;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredPostal;
import android.provider.ContactsContract.Profile;
import android.text.TextUtils;
import android.util.Log;

public class ProfileQueryUtil {

	private static final String TAG = "ProfileQueryUtil";

	public ProfileQueryUtil() {
		super();
	}

	public void queryProfile(Context context, JSONObject profile)
			throws JSONException {
		String[] projection = new String[] { Profile._ID,
				Profile.DISPLAY_NAME_PRIMARY, Profile.LOOKUP_KEY,
				Profile.PHOTO_THUMBNAIL_URI };

		Cursor cursor = context.getContentResolver().query(Profile.CONTENT_URI,
				projection, null, null, null);

		while (cursor.moveToNext()) {
			final String displayName = blankIfNull(cursor.getString(cursor
					.getColumnIndex(Profile.DISPLAY_NAME_PRIMARY)));

			final String lookupKey = blankIfNull(cursor.getString(cursor
					.getColumnIndex(Profile.LOOKUP_KEY)));

			final String thumbnailUri = blankIfNull(cursor.getString(cursor
					.getColumnIndex(Profile.PHOTO_THUMBNAIL_URI)));

			final String id = blankIfNull(cursor.getString(cursor
					.getColumnIndex(Profile._ID)));

			profile.put(Profile.DISPLAY_NAME_PRIMARY, displayName);
			profile.put(Profile.PHOTO_THUMBNAIL_URI, thumbnailUri);

			profile.put("id", id);
			profile.put("lookupKey", lookupKey);

			queryEmail(context, profile, lookupKey);
			queryPhone(context, profile, lookupKey);
			queryPostal(context, profile, lookupKey);

		}
	}

	private void queryPostal(Context context, JSONObject profile,
			String lookupKey) throws JSONException {
		JSONArray list = new JSONArray();

		String[] projection = { StructuredPostal._ID,
				StructuredPostal.LOOKUP_KEY,
				StructuredPostal.FORMATTED_ADDRESS, StructuredPostal.TYPE,
				StructuredPostal.LABEL, StructuredPostal.STREET,
				StructuredPostal.POBOX, StructuredPostal.NEIGHBORHOOD,
				StructuredPostal.CITY, StructuredPostal.REGION,
				StructuredPostal.COUNTRY, StructuredPostal.POSTCODE };

		final String selection = ContactsContract.Contacts.Data.MIMETYPE + "=?";
		final String[] selectionArgs = new String[] { StructuredPostal.CONTENT_ITEM_TYPE };

		Cursor cursor = context.getContentResolver().query(
				Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
						ContactsContract.Contacts.Data.CONTENT_DIRECTORY),
				projection, selection, selectionArgs, StructuredPostal.TYPE);

		while (cursor.moveToNext()) {

			final String formattedAddress = blankIfNull(cursor.getString(cursor
					.getColumnIndex(StructuredPostal.FORMATTED_ADDRESS)));

			final int type = cursor.getInt(cursor
					.getColumnIndex(StructuredPostal.TYPE));

			final String label = blankIfNull(cursor.getString(cursor
					.getColumnIndex(StructuredPostal.LABEL)));

			final String street = blankIfNull(cursor.getString(cursor
					.getColumnIndex(StructuredPostal.STREET)));

			final String pobox = blankIfNull(cursor.getString(cursor
					.getColumnIndex(StructuredPostal.POBOX)));

			final String neighbourhood = blankIfNull(cursor.getString(cursor
					.getColumnIndex(StructuredPostal.NEIGHBORHOOD)));

			final String city = blankIfNull(cursor.getString(cursor
					.getColumnIndex(StructuredPostal.CITY)));
			final String region = blankIfNull(cursor.getString(cursor
					.getColumnIndex(StructuredPostal.REGION)));
			final String country = blankIfNull(cursor.getString(cursor
					.getColumnIndex(StructuredPostal.COUNTRY)));
			final String postalcode = blankIfNull(cursor.getString(cursor
					.getColumnIndex(StructuredPostal.POSTCODE)));

			Address geoparsed = null;

			if (formattedAddress != null) {

				Geocoder geocoder = new Geocoder(context);
				List<Address> georesult;
				try {
					georesult = geocoder.getFromLocationName(formattedAddress,
							1);
					geoparsed = georesult.get(0);
//					System.out.println(formattedAddress);
//					System.out.println(formattedAddress + " (:) " + geoparsed);
				} catch (IOException e) {
					Log.w(TAG, "Unalbe to parse address.", e);
				}

			}

			if (geoparsed == null) {
				// XXX Make sure its not null so we don't have to check over and
				// over again.
				geoparsed = new Address(Locale.getDefault());
			}

			JSONObject entry = new JSONObject();
			entry.put("label", label);
			entry.put(
					"type",
					context.getResources().getString(
							StructuredPostal.getTypeLabelResource(type)));

			// XXX The street often ends up looking the same as the formatted
			// address. If we think its the same, then we won't use it in favour
			// of the parsed values.
			if (!TextUtils.isEmpty(formattedAddress)
					&& !formattedAddress.equals(street)) {
				entry.put("street",
						getBestGuess(street, geoparsed.getThoroughfare()));
			} else if (!TextUtils.isEmpty(street)) {

				final String value = getBestGuess(null,
						geoparsed.getSubThoroughfare())
						+ " " + getBestGuess(null, geoparsed.getThoroughfare());
				// XXX if we get this far, then we likely want to use the value,
				// however if the value is bogus, then we will fall back to the
				// original value for street, regardless of what that was.
				entry.put("street", getBestGuess(value, street));
			}

			entry.put("pobox", pobox);
			entry.put("neighbourhood",
					getBestGuess(neighbourhood, geoparsed.getSubLocality()));
			entry.put("city", getBestGuess(city, geoparsed.getLocality()));
			entry.put("region", getBestGuess(region, geoparsed.getAdminArea()));
			entry.put("country",
					getBestGuess(country, geoparsed.getCountryName()));
			entry.put("postalcode",
					getBestGuess(postalcode, geoparsed.getPostalCode()));
			entry.put("formatted", formattedAddress);

			entry.put("latitude",
					getBestGuess("", Double.toString(geoparsed.getLatitude())));
			entry.put("longitude",
					getBestGuess("", Double.toString(geoparsed.getLongitude())));

			list.put(entry);
		}

		cursor.close();

		profile.put("postal", list);
	}

	private Object getBestGuess(String provided, String parsed) {
		if (TextUtils.isEmpty(provided)) {
			return parsed;
		}
		return provided;
	}

	private void queryPhone(Context context, JSONObject profile,
			String lookupKey) throws JSONException {
		JSONArray list = new JSONArray();

		String[] projection = { Phone._ID, Phone.LOOKUP_KEY, Phone.NUMBER,
				Phone.TYPE, Phone.LABEL };

		final String selection = ContactsContract.Contacts.Data.MIMETYPE + "=?";
		final String[] selectionArgs = new String[] { Phone.CONTENT_ITEM_TYPE };

		Cursor cursor = context.getContentResolver().query(
				Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
						ContactsContract.Contacts.Data.CONTENT_DIRECTORY),
				projection, selection, selectionArgs, Phone.TYPE);
		while (cursor.moveToNext()) {

			final String number = blankIfNull(cursor.getString(cursor
					.getColumnIndex(Phone.NUMBER)));
			final String label = blankIfNull(cursor.getString(cursor
					.getColumnIndex(Phone.LABEL)));
			final int type = cursor.getInt(cursor.getColumnIndex(Phone.TYPE));

			JSONObject entry = new JSONObject();
			entry.put("number", number);
			entry.put("label", label);
			entry.put(
					"type",
					context.getResources().getString(
							Phone.getTypeLabelResource(type)));

			list.put(entry);
		}
		cursor.close();
		profile.put("phone", list);

	}

	private void queryEmail(Context context, JSONObject profile,
			String lookupKey) throws JSONException {

		JSONArray list = new JSONArray();

		// RawContacts.Data.
		String[] projection = { Email._ID, Email.LOOKUP_KEY, Email.ADDRESS,
				Email.TYPE, Email.LABEL };

		final String selection = ContactsContract.Contacts.Data.MIMETYPE + "=?";
		final String[] selectionArgs = new String[] { Email.CONTENT_ITEM_TYPE };

		Cursor cursor = context.getContentResolver().query(
				Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
						ContactsContract.Contacts.Data.CONTENT_DIRECTORY),
				projection, selection, selectionArgs, Email.TYPE);

		while (cursor.moveToNext()) {

			final String address = blankIfNull(cursor.getString(cursor
					.getColumnIndex(Email.ADDRESS)));
			final String label = blankIfNull(cursor.getString(cursor
					.getColumnIndex(Email.LABEL)));
			final int type = cursor.getInt(cursor.getColumnIndex(Email.TYPE));

			JSONObject entry = new JSONObject();
			entry.put("address", address);
			entry.put("label", label);
			entry.put(
					"type",
					context.getResources().getString(
							Email.getTypeLabelResource(type)));

			list.put(entry);
		}
		cursor.close();
		profile.put("email", list);

	}

	private String blankIfNull(String string) {
		if (string == null) {
			return "";
		}
		return string;
	}
}
