package com.sixgreen.plugins.myprofile;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Contacts.Photo;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

/**
 * Quote from the Android documentation: "Remember that you should consider a
 * user's profile to be sensitive. The permission READ_PROFILE allows you to
 * access the device user's personally-identifying data. Make sure to tell the
 * user why you need user profile access permissions in the description of your
 * application."
 * 
 * https://bitbucket.org/sixgreen/cordova-plugin-myprofile
 * 
 * @author Brill Pappin
 */
public class MyProfile extends CordovaPlugin {
	private static final String TAG = "MyProfile";

	public MyProfile() {
		super();
	}

	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) throws JSONException {

		if ("profile".equals(action)) {
			doProfile(callbackContext);
			// need to return as this call is async.
			return true;
		} else if ("thumbnail".equals(action)) {
			JSONObject options = args.optJSONObject(0);
			String photoThumbnailUri = options.optString("uri");
			doThumbnail(photoThumbnailUri, callbackContext);
			return true;
		}
		return false;

		// callbackContext.success();
		// return true;
	}

	private void doThumbnail(final String photoThumbnailUri,
			final CallbackContext callbackContext) {
		cordova.getThreadPool().execute(new Runnable() {

			@Override
			public void run() {
				if (!TextUtils.isEmpty(photoThumbnailUri)) {
					Bitmap thumbnail = loadContactPhotoThumbnail(photoThumbnailUri);
					processPicture(callbackContext, thumbnail);
				}
			}
		});

	}

	public synchronized void doProfile(final CallbackContext callbackContext) {

		cordova.getThreadPool().execute(new Runnable() {

			@Override
			public void run() {
				Log.d(TAG, "Collecting profile data for caller.");

				JSONObject profile = new JSONObject();
				// XXX check android version and don't try to run if its
				// unsupported.
				// profile.put("status", "unsupported");

				// Retrieves the profile from the Contacts Provider
				try {

					ProfileQueryUtil profileQuery = new ProfileQueryUtil();
					profileQuery.queryProfile(cordova.getActivity(), profile);

					Log.d(TAG, "Returning profile data to caller.");
					callbackContext.sendPluginResult(new PluginResult(
							PluginResult.Status.OK, profile));

				} catch (JSONException e) {
					Log.e(TAG, "Failed to process profile data.", e);
					callbackContext.sendPluginResult(new PluginResult(
							PluginResult.Status.ERROR, e.getMessage()));
				}

			}
		});

	}

	/**
	 * Compress bitmap using jpeg, convert to Base64 encoded string, and return
	 * to JavaScript.
	 * 
	 * @param bitmap
	 */
	public void processPicture(final CallbackContext callbackContext,
			Bitmap bitmap) {
		ByteArrayOutputStream jpeg_data = new ByteArrayOutputStream();
		try {
			// XXX Assume we want max quality, since this is already a
			// thumbnail.
			if (bitmap.compress(CompressFormat.JPEG, 100, jpeg_data)) {
				byte[] code = jpeg_data.toByteArray();
				byte[] output = Base64.encode(code, Base64.NO_WRAP);
				String js_out = new String(output);
				callbackContext.success(js_out);
				js_out = null;
				output = null;
				code = null;
			}
		} catch (Exception e) {
			callbackContext.error(e.getMessage());
		}
		jpeg_data = null;
	}

	/**
	 * <p>
	 * This code from
	 * http://developer.android.com/training/contacts-provider/display
	 * -contact-badge.html
	 * </p>
	 * Load a contact photo thumbnail and return it as a Bitmap, resizing the
	 * image to the provided image dimensions as needed.
	 * 
	 * @param photoData
	 *            photo ID Prior to Honeycomb, the contact's _ID value. For
	 *            Honeycomb and later, the value of PHOTO_THUMBNAIL_URI.
	 * @return A thumbnail Bitmap, sized to the provided width and height.
	 *         Returns null if the thumbnail is not found.
	 */
	private Bitmap loadContactPhotoThumbnail(String photoData) {
		// Creates an asset file descriptor for the thumbnail file.
		AssetFileDescriptor afd = null;
		// try-catch block for file not found
		try {
			// Creates a holder for the URI.
			Uri thumbUri;
			// If Android 3.0 or later
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				// Sets the URI from the incoming PHOTO_THUMBNAIL_URI
				thumbUri = Uri.parse(photoData);
			} else {
				// Prior to Android 3.0, constructs a photo Uri using _ID
				/*
				 * Creates a contact URI from the Contacts content URI incoming
				 * photoData (_ID)
				 */
				final Uri contactUri = Uri.withAppendedPath(
						Contacts.CONTENT_URI, photoData);
				/*
				 * Creates a photo URI by appending the content URI of
				 * Contacts.Photo.
				 */
				thumbUri = Uri.withAppendedPath(contactUri,
						Photo.CONTENT_DIRECTORY);
			}

			/*
			 * Retrieves an AssetFileDescriptor object for the thumbnail URI
			 * using ContentResolver.openAssetFileDescriptor
			 */
			afd = cordova.getActivity().getContentResolver()
					.openAssetFileDescriptor(thumbUri, "r");
			/*
			 * Gets a file descriptor from the asset file descriptor. This
			 * object can be used across processes.
			 */
			FileDescriptor fileDescriptor = afd.getFileDescriptor();
			// Decode the photo file and return the result as a Bitmap
			// If the file descriptor is valid
			if (fileDescriptor != null) {
				// Decodes the bitmap
				return BitmapFactory.decodeFileDescriptor(fileDescriptor, null,
						null);
			}
			// If the file isn't found
		} catch (FileNotFoundException e) {
			/*
			 * Handle file not found errors
			 */
			Log.w(TAG, "Could not load the image file.", e);

			// In all cases, close the asset file descriptor
		} finally {
			if (afd != null) {
				try {
					afd.close();
				} catch (IOException e) {
				}
			}
		}
		return null;
	}
}
